<?php
/**
 * @package segun_adeniji/company
 */

namespace Company\Model;

class CompanyE extends Company implements CriteriaInterface {
	
     /** @const array */
	 const DOORS = array(2, 3, 4, 5);
	
	/**
	 * Has property insurance property.
	 * @var bool 
	 */
	private $hasLicense;

	/**
	 * Has property insurance property.
	 * @var bool 
	 */
	private $hasCarInsurance;

	/**
	 * Number of car doors.
	 * @var int 
	 */
	private $carDoors;

	/**
	 * Class constructor.
	 * @param    bool $hasLicense The company reqired if the applicant driver's license.
	 * @param    int $carDoors [Optional] The company reqired if the applicant number of car doors he have.
	 */
	public function __construct( bool $hasLicense, int $carDoors = 4) {
		$this->setHasLicense( $hasLicense);
		$this->setCarDoor( $carDoors);
	}

	/** @return bool HasLicense */
	public function getHasLicense(): bool {
		return $this->hasLicense;
	}

	/** @param bool HasLicense */
	public function setHasLicense( bool $hasLicense): void {
		$this->hasLicense= $hasLicense;
	}
	
	/** @return int CarDoor*/
	public function getCarDoor(): int {
		return $this->carDoor;
	}

	/** @param int CarDoor */
	public function setCarDoor( int $carDoor): void {
		$this->carDoor= $carDoor;
	}

	/** @return bool for required properties  */
	public function isOk(): bool {
		return $this->getHasLicense() && in_array($this->getCarDoor(), self::DOORS);
	}
        
        function __toString(): string {
            return substr(strrchr(__CLASS__, "\\"), 1);    
        }
}