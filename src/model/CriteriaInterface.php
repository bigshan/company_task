<?php
/**
 * @package segun_adeniji/company
 */

namespace Company\Model;

/**
 * The interface for the nonces basic functionality.
 */
interface CriteriaInterface {

	/** @return bool if it meet up the criteria */
	public function isOk(): bool;
}
