<?php
/**
 * @package segun_adeniji/company
 */

namespace Company\Model;

class CompanyG extends Company implements CriteriaInterface {
	/**
	 * Has apartment or house property.
	 * @var bool 
	 */
	private $hasMassageQualificationCertificate;

	/**
	 * Has property insurance property.
	 * @var bool 
	 */
	private $hasLiabilityInsurance;

	/**
	 * Class constructor.
	 * @param    bool $hasMassageQualificationCertificate   The company reqired if the applicant has an apartment or  a house
	 * @param    bool $hasLiabilityInsurance  The company reqired if the applicant has property insurance.
	 */
	public function __construct( bool $hasMassageQualificationCertificate, bool $hasLiabilityInsurance) {
		$this->setHasMassageQualificationCertificate ( $hasMassageQualificationCertificate );
		$this->setHasLiabilityInsurance( $hasLiabilityInsurance );
	}

    /** @return bool HasMassageQualificationCertificate  */
	public function getHasMassageQualificationCertificate(): bool {
		return $this->hasMassageQualificationCertificate;
	}

	/** @param bool HasMassageQualificationCertificate  */
	public function setHasMassageQualificationCertificate( bool $hasMassageQualificationCertificate ): void {
		$this->hasMassageQualificationCertificate  = $hasMassageQualificationCertificate;
	}

	/** @return bool HasLiabilityInsurance */
	public function getHasLiabilityInsurance(): bool {
		return $this->hasLiabilityInsurance;
	}

	/** @param bool HasLiabilityInsurance */
	public function setHasLiabilityInsurance( bool $hasLiabilityInsurance): void {
		$this->hasLiabilityInsurance = $hasLiabilityInsurance;
	}

	/** @return bool for required properties  */
	public function isOk(): bool {
		return $this->getHasMassageQualificationCertificate () && $this->getHasLiabilityInsurance();
	}
        
        function __toString(): string {
            return substr(strrchr(__CLASS__, "\\"), 1);    
        }
}