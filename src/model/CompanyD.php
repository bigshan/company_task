<?php
/**
 * @package segun_adeniji/company
 */

namespace Company\Model;

class CompanyD extends Company implements CriteriaInterface {

	/** @const properties */
	const PROPERTIES = array('apartment', 'house', 'flat');

	/**
	 * Has apartment or house property.
	 * @var string 
	 */
	private $property;

	/**
	 * Class constructor.
	 * @param    string $property  The company reqired if the applicant has an apartment or  a house
	 */
	public function __construct( string $property) {
		$this->setProperty( $property );
	}

    /** @return string Property */
	public function getProperty(): string {
		return $this->property;
	}

	/** @param bool Property */
	public function setProperty( string $property ): void {
		$this->property = $property;
	}
	
	/** @return bool for required properties  */
	public function isOk(): bool {
		return in_array($this->getProperty(), self::PROPERTIES);
	}
        
        function __toString(): string {
            return substr(strrchr(__CLASS__, "\\"), 1);    
        }
}