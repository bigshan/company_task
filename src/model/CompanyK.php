<?php
/**
 * @package segun_adeniji/company
 */

namespace Company\Model;

class CompanyK extends Company implements CriteriaInterface {


	/**
	 * Has PayPal Account.
	 * @var bool 
	 */
	private $hasPaypalAccount;

	/**
	 * Class constructor.
	 * @param    bool $hasPaypalAccount  The company reqired if the applicant has paypal account
	 */
	public function __construct( bool $hasPaypalAccount) {
		$this->setHasPaypalAccount( $hasPaypalAccount );
	}

    /** @return bool HasPaypalAccount */
	public function getHasPaypalAccount(): bool {
		return $this->hasPaypalAccount;
	}

	/** @param bool HasPaypalAccount */
	public function setHasPaypalAccount( bool $hasPaypalAccount ): void {
		$this->hasPaypalAccount = $hasPaypalAccount;
	}

	/** @return bool for required properties  */
	public function isOk(): bool {
		return $this->getHasPaypalAccount();
	}
        
        function __toString(): string {
            return substr(strrchr(__CLASS__, "\\"), 1);    
        }
}