<?php

/**
 * @package segun_adeniji/company
 */

namespace Company\Model;

class Company {
    /**
     * Other company properties, methods are set here
     */
    /** @return string Class Name */
    function __toString(): string {
    	return substr(strrchr(__CLASS__, "\\"), 1);    
    }
}