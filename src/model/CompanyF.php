<?php
/**
 * @package segun_adeniji/company
 */

namespace Company\Model;

class CompanyF extends Company implements CriteriaInterface {

	/** @const properties */
	const MOTOCYCLE_TYPE = array('scooter', 'bike', 'motorcycle');

	/**
	 * Has apartment or house property.
	 * @var string 
	 */
	private $motorcycleType;

	/**
	 * Has property insurance property.
	 * @var bool 
	 */
	private $hasMotorcycleInsurance;

	/**
	 * Has property insurance property.
	 * @var bool 
	 */
	private $hasLicense;

	/**
	 * Class constructor.
	 * @param    string $motorcycleType  The company reqired if the applicant has an apartment or  a house
	 * @param    bool $hasMotorcycleInsurance  The company reqired if the applicant has property insurance.
	 * @param    bool $hasMotorcycleInsurance  The company reqired if the applicant has property insurance.
	 */
	public function __construct( string $motorcycleType, bool $hasMotorcycleInsurance, bool $hasLicense) {
		$this->setMotorcycleType( $motorcycleType );
		$this->setHasMotorcycleInsurance( $hasMotorcycleInsurance );
		$this->setHasLicense( $hasLicense);
	}

    /** @return string MotorcycleType */
	public function getMotorcycleType(): string {
		return $this->motorcycleType;
	}

	/** @param string MotorcycleType */
	public function setMotorcycleType( string $motorcycleType ): void {
		$this->motorcycleType = $motorcycleType;
	}

	/** @return bool HasMotorcycleInsurance */
	public function getHasMotorcycleInsurance(): bool {
		return $this->hasMotorcycleInsurance;
	}

	/** @param bool HasMotorcycleInsurance */
	public function setHasMotorcycleInsurance( bool $hasMotorcycleInsurance ): void {
		$this->hasMotorcycleInsurance = $hasMotorcycleInsurance;
	}

	/** @return bool HasLicense */
	public function getHasLicense(): bool {
		return $this->hasLicense;
	}

	/** @param bool HasLicense */
	public function setHasLicense( bool $hasLicense): void {
		$this->hasLicense= $hasLicense;
	}

	/** @return bool for required properties  */
	public function isOk(): bool {
		return in_array($this->getMotorcycleType(), self::MOTOCYCLE_TYPE) && $this->getHasMotorcycleInsurance() &&  $this->getHasLicense();
	}
        
        function __toString(): string {
            return substr(strrchr(__CLASS__, "\\"), 1);    
        }
}