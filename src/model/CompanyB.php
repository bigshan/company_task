<?php
/**
 * @package segun_adeniji/company
 */

namespace Company\Model;

class CompanyB extends CompanyE implements CriteriaInterface {	

	/** @const array */
	 const DOORS = array(4, 5);
	
	/**
	 * Has property insurance property.
	 * @var bool 
	 */
	private $hasCarInsurance;

	/**
	 * Class constructor.
	 * @param    bool $hasLicense The company reqired if the applicant driver's license.
	 * @param    bool $hasCarInsurance The company reqired if the applicant has property insurance.
	 * @param    int $carDoors [Optional] The company reqired if the applicant number of car doors he have.
	 */
	public function __construct( bool $hasLicense, bool $hasCarInsurance, int $carDoors = 4) {
		parent::__construct( $hasLicense, $carDoors );
		$this->setHasCarInsurance( $hasCarInsurance);
	}

	/** @return bool HasCarInsurance */
	public function getHasCarInsurance(): bool {
		return $this->hasCarInsurance;
	}

	/** @param bool HasCarInsurance */
	public function setHasCarInsurance( bool $hasCarInsurance): void {
		$this->hasCarInsurance= $hasCarInsurance;
	}

	/** @return bool for required properties  */
	public function isOk(): bool {
		return $this->getHasLicense() &&  $this->getHasCarInsurance()  && in_array( $this->getCarDoor(), self::DOORS );
	}
        
        function __toString(): string {
            return substr(strrchr(__CLASS__, "\\"), 1);    
        }
}