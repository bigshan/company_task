<?php
/**
 * @package segun_adeniji/company
 */

namespace Company\Model;

class CompanyC extends Company implements CriteriaInterface {
	/**
	 * Has apartment or house property.
	 * @var bool 
	 */
	private $hasSecurityNumber;

	/**
	 * Has property insurance property.
	 * @var bool 
	 */
	private $hasWorkPermit;

	/**
	 * Class constructor.
	 * @param    bool $hasSecurityNumber  The company reqired if the applicant has an apartment or  a house
	 * @param    bool $hasWorkPermit  The company reqired if the applicant has property insurance.
	 */
	public function __construct( bool $hasSecurityNumber, bool $hasWorkPermit) {
		$this->setHasSecurityNumber( $hasSecurityNumber );
		$this->setHasWorkPermit( $hasWorkPermit );
	}

	/** @return bool HasSecurityNumber */
	public function getHasSecurityNumber(): bool {
		return $this->hasSecurityNumber;
	}

	/** @param bool HasSecurityNumber */
	public function setHasSecurityNumber( bool $hasSecurityNumber ): void {
		$this->hasSecurityNumber = $hasSecurityNumber;
	}

	/** @return bool HasWorkPermit */
	public function getHasWorkPermit(): bool {
		return $this->hasWorkPermit;
	}

	/** @param bool HasWorkPermit */
	public function setHasWorkPermit( bool $hasWorkPermit ): void {
		$this->hasWorkPermit = $hasWorkPermit;
	}

	/** @return bool for required properties  */
	public function isOk(): bool {
		return $this->getHasSecurityNumber() && $this->getHasWorkPermit();
	}
	
	function __toString(): string {
		return substr(strrchr(__CLASS__, "\\"), 1);    
	}
}