<?php
/**
 * @package segun_adeniji/company
 */

namespace Company\Model;

class CompanyA extends CompanyD implements CriteriaInterface {

	/** @const properties */
	 const PROPERTIES = array('apartment', 'house');
	/**
	 * Has property insurance property.
	 * @var bool 
	 */
	private $hasPropertyInsurance;

	/**
	 * Class constructor.
	 * @param    string $property  The company reqired if the applicant has an apartment or  a house
	 * @param    bool $hasPropertyInsurance  The company reqired if the applicant has property insurance.
	 */
	public function __construct( string $property, bool $hasPropertyInsurance) {
		parent::__construct( $property );
		$this->setHasPropertyInsurance( $hasPropertyInsurance );
	}

	/** @return bool HasPropertyInsurance */
	public function getHasPropertyInsurance(): bool {
	    return $this->hasPropertyInsurance;
	}

	/** @param bool HasPropertyInsurance */
	public function setHasPropertyInsurance( bool $hasPropertyInsurance ): void {
	    $this->hasPropertyInsurance = $hasPropertyInsurance;
	}

	/** @return bool for required properties  */
	public function isOk(): bool {
		return in_array($this->getProperty(), self::PROPERTIES) && $this->getHasPropertyInsurance();
	}
        
        function __toString(): string {
            return substr(strrchr(__CLASS__, "\\"), 1);    
        }
}
