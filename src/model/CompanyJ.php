<?php
/**
 * @package segun_adeniji/company
 */

namespace Company\Model;

class CompanyJ extends Company implements CriteriaInterface {

	/** @return bool for required properties  */
	public function isOk(): bool {
		return TRUE;
	}
        
        function __toString(): string {
            return substr(strrchr(__CLASS__, "\\"), 1);    
        }
}