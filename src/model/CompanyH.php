<?php
/**
 * @package segun_adeniji/company
 */

namespace Company\Model;

class CompanyH extends CompanyD implements CriteriaInterface {

	/** @const properties */
	const PROPERTIES = array('storage place', 'garage');

	/**
	 * Class constructor.
	 * @param    string $property  The company reqired if the applicant has an apartment or  a house
	 */
	public function __construct( string $property ) {
		parent::__construct( $property );
	}

	/** @return bool for required properties  */
	public function isOk(): bool {
		return in_array($this->getProperty(), self::PROPERTIES);
	}
        
        function __toString(): string {
            return substr(strrchr(__CLASS__, "\\"), 1);    
        }
}