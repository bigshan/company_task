<?php
/**
 * @package segun_adeniji/company
 */

namespace Company\Controller;

use Company\Model\CompanyA;
use Company\Model\CompanyB;
use Company\Model\CompanyC;
use Company\Model\CompanyD;
use Company\Model\CompanyE;
use Company\Model\CompanyF;
use Company\Model\CompanyG;
use Company\Model\CompanyH;
use Company\Model\CompanyJ;
use Company\Model\CompanyK;

class ApplicantController {

    function index() {
        /** applicant does not have property insurance */
        $hasPropertyInsurance = false;
        /** applicant does not have property like house or flat or apartment */
        $property = 'none';
        /** applicant does not have car insurance */
        $hasCarInsurance = false;
        /** applicant does not have security insurance */
        $hasSecurityNumber = false;
        /** applicant does not have work permit */
        $hasWorkPermit = false;
        /** applicant has license */
        $hasLicense = true;
        /** applicant has no car door */
        $carDoors = 0;
        /** applicant has bike */
        $motorcycleType = 'bike';
        /** applicant does not have motocycle  insurance i */
        $hasMotorcycleInsurance = false;
        /** applicant does not have massage qualification certificate */
        $hasMassageQualificationCertificate = false;
        /** applicant does not have liability insurance */
        $hasLiabilityInsurance = false;
        /** applicant does not have paypal acount */
        $hasPaypalAccount = false;

        $companyA = new CompanyA($property, $hasPropertyInsurance);
        $this->printResponse($companyA->isOk(), $companyA);

        $companyB = new CompanyB($hasLicense, $hasCarInsurance, $carDoors);
        $this->printResponse($companyB->isOk(), $companyB);

        $companyC = new CompanyC($hasSecurityNumber, $hasWorkPermit);
        $this->printResponse($companyC->isOk(), $companyC);

        $companyD = new CompanyD($property);
        $this->printResponse($companyD->isOk(), $companyD);

        $companyE = new CompanyE($hasLicense, $carDoors);
        $this->printResponse($companyE->isOk(), $companyE);

        $companyF = new CompanyF($motorcycleType, $hasMotorcycleInsurance, $hasLicense);
        $this->printResponse($companyF->isOk(), $companyF);

        $companyG = new CompanyG($motorcycleType, $hasMotorcycleInsurance, $hasLicense);
        $this->printResponse($companyG->isOk(), $companyG);

        $companyH = new CompanyH($motorcycleType, $hasMotorcycleInsurance, $hasLicense);
        $this->printResponse($companyH->isOk(), $companyH);

        $companyJ = new CompanyJ();
        $this->printResponse($companyJ->isOk(), $companyJ);

        $companyK = new CompanyK($hasPaypalAccount);
        $this->printResponse($companyK->isOk(), $companyK);
    }

    private function printResponse( bool $result, string $className): void {
        print $result ? "<br/>Candidate can work in " . $className : "<br/>Candidate can not work in " . $className ;
    }

}
