<?php
/**
 * @package segun_adeniji/company
 */

require_once __DIR__ . '/vendor/autoload.php';

use \Company\Controller\ApplicantController;

$applicantController = new ApplicantController();
$applicantController->index();